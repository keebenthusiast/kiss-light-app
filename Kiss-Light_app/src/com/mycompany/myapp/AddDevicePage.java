package com.mycompany.myapp;

import java.util.Date;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextComponent;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.TextModeLayout;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.NumericConstraint;
import com.codename1.ui.validation.Validator;

public class AddDevicePage extends com.codename1.ui.Form{
	
	private home hm;
	private SockThing st;
	
	public AddDevicePage( home hm, SockThing st ) {
		this.hm = hm;
		this.st = st;
		init();
		
	}
	private void init() {
	//TODO: Keep the Toolbar and menue
	//Toolbar toolbar ;
	//toolbar = hm.getToolbar();
	//toolbar.setTitle("Adding A New Device");
	TextModeLayout tl = new TextModeLayout(3, 2);
	Validator val = new Validator();
	Form f = new Form("Adding A New Device", tl);
	//f.setToolbar(toolbar);
	
	f.setScrollable(false); 
	TextComponent deviceName = new TextComponent().label("Name");
	TextComponent onSignal = new TextComponent().label("On or Off Signal");
	TextComponent pulse = new TextComponent().label("Pulse");
	Button submit = new Button("Submit");
	Button cancel = new Button("Cancel");
	
	val.addConstraint(deviceName, new LengthConstraint(1));
	val.addConstraint(onSignal, new NumericConstraint(true));
	val.addConstraint(pulse, new NumericConstraint(true));
	
	f.add(tl.createConstraint().horizontalSpan(2), deviceName);
	f.add(tl.createConstraint().widthPercentage(50), onSignal);
	f.add(tl.createConstraint().widthPercentage(50), pulse);
	f.add(submit);
	f.add(cancel);
	f.setEditOnShow(deviceName.getField());
	f.show();

	submit.addActionListener((e) -> {	
		if(val.isValid()) {
			Responds responds;
			String deviceNameString = deviceName.getText();
			int pulseInt = Integer.parseInt(pulse.getText());
			int onSignalInt = Integer.parseInt(onSignal.getText());
			/* add Device here yay, hence why there is a reference to SockThing. */
			
			responds = st.AddDeviceCommand(deviceNameString, pulseInt, onSignalInt);
			
			if(responds.getCode() == 200) {
				Dialog.show("Alert Server", "Added in the Device", "OK", null);
				hm.updateDevList();
				hm.show();
			}
			else {
				Dialog.show("Error " + responds.getCode(), responds.getMessage(), "OK", null);
			}
		 
		} else {
			Dialog.show("Alert!", "The form was not filled out correctly", "OK", null);
		}
	    
	});
	cancel.addActionListener((e) -> {
		/* cancel button just returns back to home */
		hm.show();
	});
	}
}
