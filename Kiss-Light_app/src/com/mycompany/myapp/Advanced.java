package com.mycompany.myapp;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;

public class Advanced extends Command
{
	public Advanced()
	{
		// super advanced stuff this!
		super( "Advanced" );
	}
	
	public void actionPerformed( ActionEvent ae )
	{
		Command cOK = new Command( "OK" );
		Command cCancel = new Command( "Cancel" );
		Command[] cmds = new Command[] { cOK, cCancel };
		String about = "Not Yet Implemented...";
		Command c = Dialog.show( "Advanced Details", about, cmds ); 
	}
}
