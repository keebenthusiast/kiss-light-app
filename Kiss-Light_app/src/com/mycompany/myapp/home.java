package com.mycompany.myapp;

import java.util.Vector;

import com.codename1.ui.Toolbar;

public class home extends com.codename1.ui.Form {
	
	/*
	 * A pointer to home's toolbar instance.
	 */
	private final Toolbar tb = this.getToolbar();
	private About about;
	private AddDevice addDev;
	private DeleteDevice delDev;
	private Discover discover;
	private Advanced adv;
	private SockThing st;
	private String selectedDevice;   // current selected combo box item
	
    public home(SockThing st) {
        this(com.codename1.ui.util.Resources.getGlobalResources());
        
    	this.st = st;
   
        /*
         * Initialize toolbar materials.
         */
    	about = new About();
        addDev = new AddDevice( this, st );
        delDev = new DeleteDevice( this, st, gui_Combo_Box );
        discover = new Discover();
        adv = new Advanced();
        
        /*
         * get devices from list.
         */
        updateDevList();
        
        /*
         * Add the command classes to the bar itself.
         */
        tb.addCommandToLeftSideMenu( addDev );
        tb.addCommandToLeftSideMenu( delDev );
        tb.addCommandToLeftSideMenu( discover );
        tb.addCommandToLeftSideMenu( adv );
        tb.addCommandToRightBar( about );
        
        this.show();
    }
    
    public home(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
        // gui_Combo_Box.addItem("this");				// Moved above, see comment.
    }
    
//////////////////////////////////////////////////////////////-- DON'T EDIT BELOW THIS LINE!!!
    protected com.codename1.ui.ComboBox gui_Combo_Box = new com.codename1.ui.ComboBox();
    protected com.codename1.ui.Container gui_Box_Layout_X_1 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.X_AXIS));
    protected com.codename1.ui.Label gui_Label_1 = new com.codename1.ui.Label();
    protected com.codename1.components.Switch gui_Switch = new com.codename1.components.Switch();
    protected com.codename1.ui.Label gui_Label = new com.codename1.ui.Label();
    protected com.codename1.ui.Button gui_OnButton = new com.codename1.ui.Button();
    protected com.codename1.ui.Button gui_OffButton = new com.codename1.ui.Button();


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void guiBuilderBindComponentListeners() {
        EventCallbackClass callback = new EventCallbackClass();
        gui_OnButton.addActionListener(callback);
        gui_OffButton.addActionListener(callback);
        gui_Combo_Box.addActionListener(callback);
    }

    class EventCallbackClass implements com.codename1.ui.events.ActionListener, com.codename1.ui.events.DataChangedListener {
        private com.codename1.ui.Component cmp;
        public EventCallbackClass(com.codename1.ui.Component cmp) {
            this.cmp = cmp;
        }

        public EventCallbackClass() {
        }

        public void actionPerformed(com.codename1.ui.events.ActionEvent ev) {
            com.codename1.ui.Component sourceComponent = ev.getComponent();

            if(sourceComponent.getParent().getLeadParent() != null && (sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.MultiButton || sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.SpanButton)) {
                sourceComponent = sourceComponent.getParent().getLeadParent();
            }

            /////////////        Add a combo box event       ///////////////
            if(sourceComponent == gui_Combo_Box) {
            	comboBoxActionEvent(ev);
            }
            if(sourceComponent == gui_OnButton) {
                onOnButtonActionEvent(ev);
            }
            if(sourceComponent == gui_OffButton) {
                onOffButtonActionEvent(ev);
            }
        }

        public void dataChanged(int type, int index) {
        }
    }
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        guiBuilderBindComponentListeners();
        setLayout(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
        setInlineStylesTheme(resourceObjectInstance);
        setScrollableY(true);
                setInlineStylesTheme(resourceObjectInstance);
        setTitle("Kiss-Light");
        setName("home");
                gui_Combo_Box.setInlineStylesTheme(resourceObjectInstance);
        gui_Combo_Box.setInlineAllStyles("font:6.0mm; transparency:255; opacity:255;");
        gui_Combo_Box.setInlineUnselectedStyles("transparency:255; opacity:255;");
        gui_Combo_Box.setInlineSelectedStyles("transparency:255;");
        gui_Combo_Box.setInlinePressedStyles("transparency:255; opacity:255;");
        gui_Combo_Box.setName("Combo_Box");
                gui_Box_Layout_X_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Box_Layout_X_1.setName("Box_Layout_X_1");
        gui_Label.setText("Set Device:");
                gui_Label.setInlineStylesTheme(resourceObjectInstance);
        gui_Label.setName("Label");
        gui_OnButton.setText("On");
        gui_OnButton.setUIID("onButton");
                gui_OnButton.setInlineStylesTheme(resourceObjectInstance);
        gui_OnButton.setInlineAllStyles("font:8.0mm; bgColor:1e80e; fgColor:0; transparency:75; opacity:255; alignment:center;");
        gui_OnButton.setName("OnButton");
        gui_OffButton.setText("Off");
        gui_OffButton.setUIID("offButton");
                gui_OffButton.setInlineStylesTheme(resourceObjectInstance);
        gui_OffButton.setInlineAllStyles("font:8.0mm; bgColor:f48a00; fgColor:0; transparency:75; opacity:255; alignment:center;");
        gui_OffButton.setName("OffButton");
        addComponent(gui_Combo_Box);
        addComponent(gui_Box_Layout_X_1);
        gui_Label_1.setText("Toggle");
        gui_Label_1.setUIID("toggle");
                gui_Label_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Label_1.setName("Label_1");
                gui_Switch.setInlineStylesTheme(resourceObjectInstance);
        gui_Switch.setInlineAllStyles("bgColor:b48fd4;");
        gui_Switch.setName("Switch");
        gui_Box_Layout_X_1.addComponent(gui_Label_1);
        gui_Box_Layout_X_1.addComponent(gui_Switch);
        addComponent(gui_Label);
        addComponent(gui_OnButton);
        addComponent(gui_OffButton);
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
    public void comboBoxActionEvent(com.codename1.ui.events.ActionEvent ev) {
    	selectedDevice = (String) gui_Combo_Box.getSelectedItem();
    }
    
    public void onOnButtonActionEvent(com.codename1.ui.events.ActionEvent ev)
    {
    	String sendCommand = "SET " + selectedDevice + " ON KL/0.2\n";
    	st.receiveResponse(sendCommand);
    }
    
    public void onOffButtonActionEvent(com.codename1.ui.events.ActionEvent ev)
    {
    	String sendCommand = "SET " + selectedDevice + " OFF KL/0.2\n";
    	st.receiveResponse(sendCommand);
    }
    
    /*
     * Update a Device List periodically,
     * like when a device is added or deleted.
     */
    public void updateDevList()
    {
    	/* Remove currently selected item */
    	gui_Combo_Box.getModel().removeItem( gui_Combo_Box.getModel().getSelectedIndex() );
    	
    	/* Remove the rest as well, the list will get repopulated */
    	int sz = gui_Combo_Box.getModel().getSize();
    	for ( int i = 0; i < sz; i++ )
    	{
    		gui_Combo_Box.getModel().removeItem( gui_Combo_Box.getModel().getSelectedIndex() );
    	}
    	
    	/* Same as before */
    	String init = st.receiveResponse( "LIST KL/0.2\n" );
        st.parseDeviceBuf( init );
        
        for(int x = 0; x < st.getDeviceCount() - 1; x++) 	
        {
        	gui_Combo_Box.addItem(st.getParsedDevice(x)); // Moved to this home() call to have access to instantiated SocketThing object. 
        }                                                 // home(resourceObjectInstance) is called first and cannot have this in it's
		  												  // constructor.
        
        /* Make sure that selectedDevice is also correctly selected as well */
        selectedDevice = (String) gui_Combo_Box.getSelectedItem();
    }
}