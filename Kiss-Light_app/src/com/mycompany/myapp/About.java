package com.mycompany.myapp;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;

/*
 * Just an about, can be customized later.
 */
public class About extends Command
{
	public About()
	{
		super( "About" );
	}
	
	public void actionPerformed( ActionEvent ae )
	{
		Command cOK = new Command( "OK" );
		Command cCancel = new Command( "Cancel" );
		Command[] cmds = new Command[] { cOK, cCancel };
		String about = "The Mad Engineers\nSenior Project 2020\nKiss-Light App version 0.0.1\nExample thing.";
		Command c = Dialog.show( "About", about, cmds ); 
	}
}