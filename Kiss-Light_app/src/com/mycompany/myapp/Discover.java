package com.mycompany.myapp;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;

public class Discover extends Command 
{
	public Discover()
	{
		super( "discover" );
	}
	
	public void actionPerformed( ActionEvent ae )
	{
		Command cOK = new Command( "OK" );
		Command cCancel = new Command( "Cancel" );
		Command[] cmds = new Command[] { cOK, cCancel };
		String about = "The CodeName One framework doesn't support UDP, so this may not ever get implemented...";
		Command c = Dialog.show( "Discovery", about, cmds ); 
	}
}
