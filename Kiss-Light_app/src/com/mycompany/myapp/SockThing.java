package com.mycompany.myapp;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.String;
import java.util.StringTokenizer;
import com.codename1.io.Socket;
import com.codename1.io.SocketConnection;

/*
 * The primary task of this class is to unify all things
 * related to network sockets.
 * 
 */

public class SockThing extends SocketConnection
{
	/*
	 * Useful Constants
	 */
	private final String IP_ADDR = "10.0.0.23";
	private final int PORT = 1155;
	private final int BUFFER_SIZE = 2048;
	
	/*
	 * Fields
	 */
	private Socket sock = null;
	private InputStream in = null;
	private OutputStream out = null; 
	private byte[] buf = new byte[BUFFER_SIZE];
	private String[] parsedDevices;			// Array for parsed server response of listed devices
	private int deviceCount;				// Device count
	
	/*
	 * Methods
	 */
	public SockThing()
	{
		Socket.connect(IP_ADDR, PORT, this);
	}
	
	@Override
	public void connectionEstablished( InputStream is, OutputStream os )
	{
		System.out.println( "connected to server" );
		System.out.println( "Socket is connected? " + this.isConnected() );
		
		this.in = is;
		this.out = os;
	}
	
	/* 
	 * Function for parsing buf to obtain devices. 
	 * Will eventually be in a new Device class.   
	 * 
	 */
	public Responds AddDeviceCommand(String name, int pulse, int code	)
	{
		Responds resp = new Responds();
		
		/* On or Off codes respectively */
		if ( ((code & 15) == 3) || ((code & 15) == 12) )
		{
			resp = parseResponds( "ADD " + name + " " + code + " " + pulse + " KL/0.2\n" );
		}
		else
		{
			resp = parseResponds( "KL/0.2 " + 1 + " incorrect code\n" );
		}
		
		return resp;
	}
	
	public void parseDeviceBuf(String toParse) 
	{
		StringTokenizer st = new StringTokenizer(toParse);
		int tokenCount = st.countTokens();
		setDeviceCount(tokenCount - 6);		// removing {KL/0.2 200 Number of Devices #}
		parsedDevices = new String[getDeviceCount() - 1];
		
		for(int y = 0; y < 6; y++) 
		{
			st.nextToken();					// Skipping to first device listed
		}
		for(int x = 0; x < getDeviceCount() - 1; x++) 
		{
			setParsedDevice(x, st.nextToken());
		}
		
	}
	
	/* Setter for parsedDevice array */
	
	public void setParsedDevice(int position, String deviceName) 
	{
		parsedDevices[position] = deviceName;
	}
	
	/* Getter for parsedDevice array */
	
	public String getParsedDevice(int position) 
	{
		return parsedDevices[position];
	}
	
	/* Setter for deviceCount */
	
	public void setDeviceCount(int numDevices)
	{
		deviceCount = numDevices;
	}
	
	/* Getter for deviceCount */
	
	public int getDeviceCount() 
	{
		return deviceCount;
	}
	
	/* 
	 * End functions for soon-to-be Device class
	 */
	
	@Override
	public void connectionError( int errorCode, String message )
	{
		System.out.println( "Unable to connect to server" );
	}
	
	/*
	 * Send desired commands, and return the response!
	 */
	public String receiveResponse( String sendCommand )
	{
		String response = "";
		
		/* Send command */
		try
		{
			this.buf = new byte[2048];
			buf = sendCommand.getBytes();
			out.write(buf);
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
		/* Get Response */
		try
		{
			this.buf = new byte[2048];
			in.read( buf );
			response = new String( buf );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
		return response;
		
	}
	
	//this function is to return a responds object that parses the string returned from
	//receiveResponse. Makes it easier get Code, Message and Version
	public Responds parseResponds(String command)
	{
		Responds responds = new Responds();
		String toParse, message;
		int code;
		float version;
		
		try {
			toParse = receiveResponse(command);
			String[] parsedArr = splitStr(toParse, " ");
			code = parseCode(parsedArr);
			message = parseMessage(parsedArr);
			version = parseVersion(parsedArr);
		}catch(Exception e) {
			//TODO: Change this to an actual error code
			code = 100;
			message = "Unable to parse server responds";
			version = (float) 0.2;
		}
		responds.setCode(code);
		responds.setMessage(message);
		responds.setVersion(version);
		return responds;
		
	}
	
	public int parseCode(String responds)
	{
		int code;
		String[] arrStr = splitStr(responds, " ");
		code = Integer.parseInt(arrStr[1]);
		return code;
	}
	
	public String parseMessage(String responds)
	{
		String message = "";
		String[] arrStr = splitStr(responds, " ");
		
		//TODO: Switch over to using string builder for efficiency, currently not implmented in Codename One
		/*StringBuilder builder = new StringBuilder();
		for(int i = 2; i < arrStr.length; i++) {
		    builder.append(arrStr[i]);
		}
		message = builder.toString();
		*/
		for(int i = 2; i < arrStr.length; i++) {
		    message += arrStr[i];
		}
		return message;
	}
	
	public float parseVersion(String responds)
	{
		float version;
		String subString;
		
		String[] arrStr = splitStr(responds, " ");
		subString = arrStr[0];
		String[] arrStr2 = splitStr(subString, "/");
		version = Float.parseFloat(arrStr2[1]);
		
		return version;
	}
	
	//For a responds that was already parsed
	public int parseCode(String[] responds)
	{
		int code;
		code = Integer.parseInt(responds[1]);
		return code;
	}
	
	//For a responds that was already parsed
	public String parseMessage(String[] responds)
	{
		String message = "";
		
		//TODO: Switch over to using string builder for efficiency, currently not implmented in Codename One
		/*StringBuilder builder = new StringBuilder();
		for(int i = 2; i < arrStr.length; i++) {
		    builder.append(arrStr[i]);
		}
		message = builder.toString();
		*/
		for(int i = 2; i < responds.length; i++) {
			//to add in the spaces
			if(i < responds.length)
			{
				message += responds[i] + " ";
			}else {
				message += responds[i];
			}
		}
		return message;
	}
	
	//For a responds that was already parsed
	public float parseVersion(String[] responds)
	{
		float version;
		String subString;

		subString = responds[0];
		String[] arrStr2 = splitStr(subString, "/");
		version = Float.parseFloat(arrStr2[1]);
		
		return version;
	}
	//to split the string. Current version of Java does not have the split function
	public String[] splitStr(String str, String delim) {
        StringTokenizer stringTokenizer = new StringTokenizer( str, delim );
        String[] strArr = new String[stringTokenizer.countTokens()];
        int i = 0;
        while( stringTokenizer.hasMoreTokens() ) {
            strArr[i] = stringTokenizer.nextToken();
            i++;
        }
        return strArr;
    }
}