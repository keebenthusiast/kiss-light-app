package com.mycompany.myapp;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionEvent;


public class DeleteDevice extends Command 
{
	private com.codename1.ui.ComboBox cbx;
	private SockThing st;
	private home hm;
	
	public DeleteDevice( home hm, SockThing st, com.codename1.ui.ComboBox cbx )
	{
		super( "Delete Device" );
		this.st = st;
		this.cbx = cbx;
		this.hm = hm;
	}
	
	public void actionPerformed( ActionEvent ae )
	{
		/* This would initiate the deletion of the device, and update device list */
		Command cOK = new Command( "OK" ) {
			public void actionPerformed( ActionEvent ae )
			{
				String deleteDev = "DELETE " + cbx.getSelectedItem() + " KL/0.2\n";
				String tst = st.receiveResponse( deleteDev );
				hm.updateDevList();
			}
		};
		
		/* Does absolutely nothing */
		Command cCancel = new Command( "Cancel" );
		
		Command[] cmds = new Command[] { cOK, cCancel };
		String delDev = "Are you sure you want to delete device \"" + cbx.getSelectedItem() + "\" ?";
		Command c = Dialog.show( "Delete Device", delDev, cmds ); 
	}
}
