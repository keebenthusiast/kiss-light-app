package com.mycompany.myapp;

public class Responds {

	private int code;
	private String message;
	private float version;
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public float getVersion() {
		return version;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setVersion(float version) {
		this.version = version;
	}
	
	

}
