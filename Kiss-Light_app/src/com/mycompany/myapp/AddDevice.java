package com.mycompany.myapp;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;

public class AddDevice extends Command
{
	private AddDevicePage addDevicePage;
	private SockThing st;
	private home hm;
	
	public AddDevice( home hm, SockThing st )
	{
		super( "Add Device" );
		this.hm = hm;
		this.st = st;
	}
	
	public void actionPerformed( ActionEvent ae )
	{
		//Here we can call the page
		addDevicePage = new AddDevicePage( hm, st );
		
		//Then on submit to do the socket calling and stuff
		
		/*
		Command cOK = new Command( "OK" );
		Command cCancel = new Command( "Cancel" );
		Command[] cmds = new Command[] { cOK, cCancel };
		String about = "This will have the job of adding a device for us";
		Command c = Dialog.show( "Add Device", about, cmds ); 
		*/
	}
}
